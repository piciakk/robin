#! /usr/local/bin/python3
import os as opSys
def runcmd(commandToRun):
    opSys.system(commandToRun)
brewOutPut = opSys.popen("type -a brew").read()
if brewOutPut == "-bash: type: brew: not found":
    runcmd('/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"')
runcmd("brew install portaudio")
runcmd("pip3 install pyaudio")
runcmd("pip3 install SpeechRecognition")
import speech_recognition as sr
def writeIn(stringToWriteIn):
    print(stringToWriteIn)
    opSys.system("say "+stringToWriteIn)
def getInput():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Kérlek, mondj valamit")
        audio = r.listen(source)
    result = r.recognize_google(audio, language="hu")
    return result
runcmd("clear")
print("ROBIN v1.0")
print("Developed by PiciAkk.")
def main():
    mainUserInput = getInput().lower()
    if "szia" in mainUserInput or "hali" in mainUserInput or "köszöntelek" in mainUserInput or "üdv" in mainUserInput or "cső" in mainUserInput or "helló" in mainUserInput:
        writeIn("Köszöntelek, ROBIN vagyok!")
        yesNoQuestion = None
        main()
    elif "asd" in mainUserInput:
        writeIn("Sajnálom, de nem tudok hülyeségekre válaszolni!")
        yesNoQuestion = None
        main()
    else:
        writeIn("Sajnálom, de nem ismertem fel, amit mondasz! Próbálkozz újra!")
        writeIn("Egyébként ezt mondtad: "+mainUserInput)
main()
